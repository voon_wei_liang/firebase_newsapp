//
//  ViewController.m
//  firebase_newsapp
//
//  Created by Hoi on 9/29/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)postNews:(id)sender {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    Firebase *firebase = [[Firebase alloc] initWithUrl:@"https://blistering-fire-9177.firebaseio.com/"];
    [[firebase childByAutoId] setValue:@{@"author" : @"Eve", @"content": self.contentInput.text, @"datetime":[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]],@"title" : self.titleInput.text}];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
