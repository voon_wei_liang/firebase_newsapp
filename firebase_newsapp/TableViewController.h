//
//  TableViewController.h
//  firebase_newsapp
//
//  Created by Hoi on 9/29/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>


@interface TableViewController : UITableViewController{
    NSString *name;
    Firebase *firebase;
    NSMutableArray *newsArray;
}
- (IBAction)addButton:(id)sender;

@end
