//
//  ViewController.h
//  firebase_newsapp
//
//  Created by Hoi on 9/29/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *titleInput;
@property (weak, nonatomic) IBOutlet UITextField *contentInput;

- (IBAction)postNews:(id)sender;


@end
