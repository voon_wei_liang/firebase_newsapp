//
//  TableViewController.m
//  firebase_newsapp
//
//  Created by Hoi on 9/29/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"
#import "DetailViewController.h"


@interface TableViewController ()

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    firebase = [[Firebase alloc] initWithUrl:@"https://blistering-fire-9177.firebaseio.com/"];
    newsArray=[[NSMutableArray alloc]init];
    name=@"Adam";
    NSLog(name);
    
    //input into firebase with id
//    [[firebase childByAutoId] setValue:@{@"name" : @"somename", @"text": @"sometext"}];
    
    [firebase observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot){
        [newsArray addObject:snapshot.value];
        NSLog(@"ouch");
        [self.tableView reloadData];
    }];
    [firebase observeSingleEventOfType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        // Reload the table view so that the intial messages show up
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return newsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    int row=(int)[indexPath row];
    //cell.newsLabel.text=@"hello";
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
//    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSDictionary* chatMessage = [newsArray objectAtIndex:row];
//    NSLog(@"%i %@",row,chatMessage[@"text"]);
    cell.newsLabel.text = chatMessage[@"title"];
    cell.authorLabel.text = chatMessage[@"name"];
    cell.dateTimeLabel.text =chatMessage[@"datetime"];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue identifier] isEqualToString:@"DetailSegue"]){

        DetailViewController *detailView=[segue destinationViewController];
        NSIndexPath *myindexPath=[self.tableView indexPathForSelectedRow];
        int row=(int)[myindexPath row];
        NSDictionary* chatMessage = [newsArray objectAtIndex:row];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        int arrsize=detailView.DetailModal.count;
        detailView.DetailModal=@[chatMessage[@"title"],chatMessage[@"content"],chatMessage[@"author"],[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]]];
    }
}


- (IBAction)addButton:(id)sender {
    [[firebase childByAutoId] setValue:@{@"name" : @"Eve", @"text": @"sometext"}];
}
@end
