//
//  TableViewCell.h
//  firebase_newsapp
//
//  Created by Hoi on 9/29/15.
//  Copyright © 2015 Hoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *newsLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;

@end
